package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CounterTest
{
	@Test
	public void calculateTest()
	{
		int k = new Counter().calculate(2,0);
		assertEquals("div",Integer.MAX_VALUE,k);
	}	
      @Test
      public void calculateTest2()
        {
                int k = new Counter().calculate(2,1);
                assertEquals("div",2,k);
        }

}
