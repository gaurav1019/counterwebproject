package com.qaagility.controller;

public class Counter {

    public int calculate(int numerator, int denominator) {
        if (denominator == 0){
            return Integer.MAX_VALUE;
			}
        else{
            return numerator / denominator;
		}
    }

}
